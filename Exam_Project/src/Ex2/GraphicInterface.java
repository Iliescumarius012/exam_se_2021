package Ex2;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class GraphicInterface extends JFrame{
    JTextField textField1=new JTextField();
    JTextField textField2=new JTextField();
    JButton button=new JButton();
    final String text = "Constant text";
    boolean firstIsCurrent = true;
    GraphicInterface()
    {
        setVisible(true);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setSize(500,500);
        setTitle("Ex2");
        init();
    }
    public void init()
    {
        setLayout(null);

        textField1.setBounds(100,100,250,40);
        textField2.setBounds(100,145,250,40);
        button.setBounds(150,200,150,40);
        button.setText("Press");

        add(button);add(textField1);add(textField2);
        button.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if(firstIsCurrent) {
                    textField1.setText(text);
                    textField2.setText("");
                    firstIsCurrent = false;
                }
                else{
                    textField2.setText(text);
                    textField1.setText("");
                    firstIsCurrent = true;
                }
            }
        });

    }
    public static void main(String[] args) {
        new GraphicInterface();
    }
}
